package com.tfnsw.endpoint;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.transportnsw.InputSOATest;
import com.transportnsw.ObjectFactory;
import com.transportnsw.OutputSOATest;
import com.soapfault.error.ServiceFault;
import com.soapfault.error.ServiceFaultException;

@Endpoint
public class WebServiceEndpoint {

	private static final String NAMESPACE_URI = "http://transportnsw.com";

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "inputSOATest")
	@ResponsePayload
	public OutputSOATest hello(@RequestPayload InputSOATest request) {

		String outputEmployeeID = request.getEmployeeID();
		
		String employeeName = "Test Transport Employee";
		String employeeAddress = "457 Pitt Street, NSW, 2000";
		String employeeTelephone = "0422316345";
		String employeeDistance = "50 Km";

		if (outputEmployeeID.equals("12345"))
		{
			employeeName = "Test Transport Employee";
			employeeAddress = "457 Pitt Street, NSW, 2000";
			employeeTelephone = "0422316345";
			employeeDistance = "50 Km";
		}
		else if (outputEmployeeID.equals("123456"))
		{
			employeeName = "Test Transport Employee 2";
			employeeAddress = "457 Pitt Street, NSW, 2000";
			employeeTelephone = "0464282839";
			employeeDistance = "20 Km";
		}
		else
		{
			throw new ServiceFaultException("ERROR",new ServiceFault(
	                "NOT_FOUND", "Employee with id: " + request.getEmployeeID() + " not found."));
	    }
		
		ObjectFactory factory = new ObjectFactory();
		OutputSOATest response = factory.createOutputSOATest();
		response.setEmployeeID(outputEmployeeID);
		response.setEmployeeName(employeeName);
		response.setEmployeeAddress(employeeAddress);
		response.setEmployeeTelephone(employeeTelephone);
		response.setEmployeeDistance(employeeDistance);

		return response;
	}
}
